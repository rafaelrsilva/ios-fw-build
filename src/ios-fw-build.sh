# Custom build para Framework iOS

# Configura o bash para encerrar imediatamente caso ocorra um erro
set -e

# Configurar o nome do projeto e o diretório do arquivo gerado.
FRAMEWORK_PROJ_NAME="RRHttpServices"
OUTPUT_DIR="${PROJECT_DIR}/../../bin"

# Outras váriáveis utilizadas no script. Não é necessário alterar essas variáveis
BUILD_DIR="${SRCROOT}/build"
FRAMEWORK_INPUT_DEVICE_FILE="${BUILD_DIR}/Release-iphoneos/${FRAMEWORK_PROJ_NAME}.framework"
FRAMEWORK_INPUT_DEVICE_BIN="${FRAMEWORK_INPUT_DEVICE_FILE}/${FRAMEWORK_PROJ_NAME}"
FRAMEWORK_INPUT_SIMULATOR_FILE="${BUILD_DIR}/Release-iphonesimulator/${FRAMEWORK_PROJ_NAME}.framework"
FRAMEWORK_INPUT_SIMULATOR_BIN="${FRAMEWORK_INPUT_SIMULATOR_FILE}/${FRAMEWORK_PROJ_NAME}"
FRAMEWORK_OUTPUT_FILE="${OUTPUT_DIR}/${FRAMEWORK_PROJ_NAME}.framework"
FRAMEWORK_OUTPUT_BIN="${FRAMEWORK_OUTPUT_FILE}/${FRAMEWORK_PROJ_NAME}"

# Se houver dados de builds anteriores, os mesmos serão deletados.
if [ -d "${BUILD_DIR}" ]; then
rm -rf "${BUILD_DIR}"
fi

# Executa o build do framework para device e simulador, usando todas as arquiteturas necessárias.
xcodebuild -target "${FRAMEWORK_PROJ_NAME}" -configuration Release -arch arm64 -arch armv7 -arch armv7s only_active_arch=no defines_module=yes -sdk "iphoneos"
xcodebuild -target "${FRAMEWORK_PROJ_NAME}" -configuration Release -arch x86_64 -arch i386 only_active_arch=no defines_module=yes -sdk "iphonesimulator"

mkdir -p "${OUTPUT_DIR}"

# Remove .framework antigo 
if [ -d "${FRAMEWORK_OUTPUT_FILE}" ]; then
rm -rf "${FRAMEWORK_OUTPUT_FILE}"
fi

# Copia o .framework para o diretório de saída
cp -r "${FRAMEWORK_INPUT_DEVICE_FILE}" "${FRAMEWORK_OUTPUT_FILE}"

# Faz um merge dos executáveis gerados para device e simulador
lipo -create -output "${FRAMEWORK_OUTPUT_BIN}" "${FRAMEWORK_INPUT_DEVICE_BIN}" "${FRAMEWORK_INPUT_SIMULATOR_BIN}"

# Copia o arquivo .swiftmodule para o arquivo de saída.
if [ -d "${FRAMEWORK_INPUT_SIMULATOR_FILE}/Modules/${FRAMEWORK_PROJ_NAME}.swiftmodule/" ]; then
cp -r "${FRAMEWORK_INPUT_SIMULATOR_FILE}/Modules/${FRAMEWORK_PROJ_NAME}.swiftmodule/" "${FRAMEWORK_OUTPUT_FILE}/Modules/${FRAMEWORK_PROJ_NAME}.swiftmodule"
fi

# Remove os builds gerado.
if [ -d "${BUILD_DIR}" ]; then
rm -rf "${BUILD_DIR}"
fi